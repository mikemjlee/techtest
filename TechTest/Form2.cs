﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TechTest
{
    public partial class Form2 : Form
    {
        public string DocumentType { get; set; }
        public string DocumentNumber { get; set; }

        public Form2()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (inputDocNum.Text != "" && cbDocType.Text != "")
            {
                DocumentType = cbDocType.Text;
                DocumentNumber = inputDocNum.Text;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
