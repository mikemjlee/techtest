﻿using System;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Collections.Generic;
using iTextSharp.text.pdf.parser;
using System.Xml;

namespace TechTest
{
    class TechTestParser
    {

        public const string TECHTEST_ERROR_FOLDER = @"Errors";

        public const string TECHTEST_DOCTYPE_INVOICE = @"Tax Invoice";
        public const string TECHTEST_DOCTYPE_NOTE = @"Credit Note";

        private string m_strSrcPath;
        public DateTime FileCreateTime { get; }

        public TechTestParser(string srcPath)
        {

            Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);

            m_strSrcPath = srcPath;
            FileCreateTime = File.GetCreationTime(srcPath);

        }

        public class RectAndText
        {
            public iTextSharp.text.Rectangle Rect;
            public String Text;
            public RectAndText(iTextSharp.text.Rectangle rect, String text)
            {
                this.Rect = rect;
                this.Text = text;
            }

            // check if the text is inside the rect
            public bool isInside(Rectangle rect)
            {

                return this.Rect.Top <= rect.Top && this.Rect.Right <= rect.Right && this.Rect.Left >= rect.Left && this.Rect.Bottom >= rect.Bottom;
            }
        }

        public class MyLocationTextExtractionStrategy : LocationTextExtractionStrategy
        {
            //Hold each coordinate
            public List<RectAndText> myPoints = new List<RectAndText>();

            //Automatically called for each chunk of text in the PDF
            public override void RenderText(TextRenderInfo renderInfo)
            {
                base.RenderText(renderInfo);

                //Get the bounding box for the chunk of text
                var bottomLeft = renderInfo.GetDescentLine().GetStartPoint();
                var topRight = renderInfo.GetAscentLine().GetEndPoint();

                //Create a rectangle from it
                var rect = new iTextSharp.text.Rectangle(
                                                        bottomLeft[Vector.I1],
                                                        bottomLeft[Vector.I2],
                                                        topRight[Vector.I1],
                                                        topRight[Vector.I2]
                                                        );

                //Add this to our main collection
                this.myPoints.Add(new RectAndText(rect, renderInfo.GetText()));
            }
        }

        public List<string>  ProcessSplitAndSave(string outputPath) 
        {
            TechTestLog.Instance.Log(TechTestLog.LOGLV, "Start ProcessSplitAndSave : process PDF file " + m_strSrcPath + " and save to " + outputPath);
            PdfReader pReader = new PdfReader(m_strSrcPath);
            List<string> aryErrorPDFPath = new List<string>();


            using (pReader)
            {
                for (int i = 1; i <= pReader.NumberOfPages; i++)
                {
                    aryErrorPDFPath.AddRange(this.SplitAndSave(pReader, outputPath, i, FileCreateTime));
                }
            }
            TechTestLog.Instance.Log(TechTestLog.LOGLV, "End ProcessSplitAndSave");
            return aryErrorPDFPath;
        }

        private List<string> SplitAndSave(PdfReader reader, string outputPath, int page, DateTime createdTimestamp)
        {

            TechTestLog.Instance.Log(TechTestLog.LOGLV, string.Format("Start SplitAndSave : split page {0} from PDF file {1} ", page, m_strSrcPath));
            //position to find document type
            iTextSharp.text.Rectangle pDocTypePosition = new iTextSharp.text.Rectangle(340, 790, 590, 820);

            MyLocationTextExtractionStrategy pLocationStrategy = new MyLocationTextExtractionStrategy();
            var pExtractor = PdfTextExtractor.GetTextFromPage(reader, page, pLocationStrategy);



            List<string> aryErrorPDFPath = new List<string>();
            string strDocumentType = "";
            string strDocumentNumber = "";


            //Loop through each chunk found
            foreach (var pText in pLocationStrategy.myPoints)
            {
                if (pText.isInside(pDocTypePosition))
                {
                    string[] arySeparator = pText.Text.Split(new[] { ": " }, StringSplitOptions.None);
                    try
                    {
                        strDocumentType = arySeparator[0].Trim();

                        if (strDocumentType == TECHTEST_DOCTYPE_INVOICE || strDocumentType == TECHTEST_DOCTYPE_NOTE)
                        {
                            strDocumentNumber = arySeparator[1].Trim();
                        }
                    }
                    catch(Exception e)
                    {
                        TechTestLog.Instance.Log(TechTestLog.ERRLV, string.Format("Split pdf tag {0} cause an error, ({1})", pText.Text, e));
                    }
                    break;
                }
            }

            Document pDocument = new Document();

            //create file path and file name with given naming convention
            string strFileName = TechTestParser.GetFileName(strDocumentType, strDocumentNumber, createdTimestamp);
            string strDestFolderPath = TechTestParser.GetFolderPath(outputPath, strDocumentType, strDocumentNumber, createdTimestamp);
            bool bExists = Directory.Exists(strDestFolderPath);

            if (!bExists)
                Directory.CreateDirectory(strDestFolderPath);

            if (strDocumentNumber == "") {

                TechTestLog.Instance.Log(TechTestLog.ERRLV, "Document can not be determined, save the file to Errors");
                aryErrorPDFPath.Add(strDestFolderPath + strFileName + ".pdf");
            }

            TechTestLog.Instance.Log(TechTestLog.LOGLV, "Save PDF file to " + strDestFolderPath + strFileName + ".pdf");
            PdfCopy pCopy = new PdfCopy(pDocument, new FileStream(strDestFolderPath + strFileName + ".pdf", FileMode.Create));
            pDocument.Open();
            pCopy.AddPage(pCopy.GetImportedPage(reader, page));
            pDocument.Close();

            //create xml file
            XmlDocument pXMLDocument = this.CreateXMLDocument(page, pLocationStrategy.myPoints);
            pXMLDocument.Save(strDestFolderPath + strFileName + ".xml");
            TechTestLog.Instance.Log(TechTestLog.LOGLV, "Save XML file to " + strDestFolderPath + strFileName + ".xml");


            TechTestLog.Instance.Log(TechTestLog.LOGLV, "End SplitAndSave");
            return aryErrorPDFPath; 
        }


        private XmlDocument CreateXMLDocument(int pagenumber, List<RectAndText> myPoints)
        {

            XmlDocument pXMLdoc = new XmlDocument();

            XmlElement pDocumentElement = pXMLdoc.CreateElement("document");
            pXMLdoc.AppendChild(pDocumentElement);

            XmlElement pPageElement = pXMLdoc.CreateElement("page");

            pPageElement.SetAttribute("index", pagenumber.ToString());
            pDocumentElement.AppendChild(pPageElement);

            foreach (var pText in myPoints)
            {

                XmlElement pCellElement = pXMLdoc.CreateElement("cell");
                pCellElement.InnerText = pText.Text;
                pPageElement.AppendChild(pCellElement);
            }

            return pXMLdoc;
        }

        public static string GetFileName(string documentType, string documentNumber, DateTime createdTimestamp)
        {
            string strSeparator = System.IO.Path.DirectorySeparatorChar.ToString();
            return (documentNumber != "") ? string.Format("{0}_{1}_{2}", documentType, documentNumber, createdTimestamp.ToString("yyyy-MM-dd HH_mm_ss")) : string.Format("ERROR_{0}", createdTimestamp.ToString("yyyy-MM-dd HH_mm_ss"));
        }

        public static string GetFolderPath(string outputPath, string documentType, string documentNumber, DateTime createdTimestamp)
        {
            string strSeparator = System.IO.Path.DirectorySeparatorChar.ToString();
            return (documentNumber != "") ? string.Format("{0}Processed" + strSeparator + "{1}" + strSeparator + "{2}" + strSeparator + "{3}" + strSeparator + "{4}" + strSeparator, outputPath, documentType, createdTimestamp.ToString("yyyy"), createdTimestamp.ToString("MM"), createdTimestamp.ToString("dd")) : string.Format("{0}Errors" + strSeparator, outputPath);
        }

    }
}
