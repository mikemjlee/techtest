﻿using System.IO;
using System;

namespace TechTest {
    
    public sealed class TechTestLog
    {
        public static int LOGLV = 0;
        public static int ERRLV = 1;

        public static TechTestLog Instance { get { return lazy.Value; } }

        private static readonly Lazy<TechTestLog> lazy = new Lazy<TechTestLog>(() => new TechTestLog());

        private TextWriter m_pTextWriter;

        private int m_nLogLevel;

        private TechTestLog()
        {
            this.m_pTextWriter = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "log.txt");
            m_nLogLevel = 0x1 << LOGLV | 0x1 << ERRLV;
        }

        public void Log(int logLevel, string format, params object[] objs)
        {
            string strLogMessage = string.Format(format, objs);
            this.Log(logLevel, strLogMessage);
        }

        public void Log(int logLevel, string logMessage)
        {
            if ((m_nLogLevel & 0x1 << logLevel) > 0)  {
                m_pTextWriter.Write("\r\n{0} Entry : ", (logLevel == LOGLV) ? "Log" : "Error");
                m_pTextWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                m_pTextWriter.WriteLine("  :");
                m_pTextWriter.WriteLine("  :{0}", logMessage);
                m_pTextWriter.WriteLine("-------------------------------");
                m_pTextWriter.Flush();
            }
        }

        public void SetLogLevel(int logLevel) {

            m_nLogLevel = logLevel;

        }

        ~TechTestLog(){
            try
            {
                m_pTextWriter.Close();
            } catch
            {
            }
        }
    
    }



}