﻿namespace TechTest
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDocumentType = new System.Windows.Forms.Label();
            this.lblDocumentNum = new System.Windows.Forms.Label();
            this.lblDesc = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.cbDocType = new System.Windows.Forms.ComboBox();
            this.inputDocNum = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblDocumentType
            // 
            this.lblDocumentType.AutoSize = true;
            this.lblDocumentType.Location = new System.Drawing.Point(23, 44);
            this.lblDocumentType.Name = "lblDocumentType";
            this.lblDocumentType.Size = new System.Drawing.Size(106, 15);
            this.lblDocumentType.TabIndex = 0;
            this.lblDocumentType.Text = "Document Type :";
            // 
            // lblDocumentNum
            // 
            this.lblDocumentNum.AutoSize = true;
            this.lblDocumentNum.Location = new System.Drawing.Point(23, 83);
            this.lblDocumentNum.Name = "lblDocumentNum";
            this.lblDocumentNum.Size = new System.Drawing.Size(123, 15);
            this.lblDocumentNum.TabIndex = 1;
            this.lblDocumentNum.Text = "Document Number :";
            // 
            // lblDesc
            // 
            this.lblDesc.AutoSize = true;
            this.lblDesc.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblDesc.Location = new System.Drawing.Point(12, 9);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(423, 15);
            this.lblDesc.TabIndex = 2;
            this.lblDesc.Text = "Please select the Document Type and enter Document Number ";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(340, 111);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cbDocType
            // 
            this.cbDocType.FormattingEnabled = true;
            this.cbDocType.Items.AddRange(new object[] {
            "Tax Invoice",
            "Credit Note"});
            this.cbDocType.Location = new System.Drawing.Point(154, 41);
            this.cbDocType.Name = "cbDocType";
            this.cbDocType.Size = new System.Drawing.Size(261, 23);
            this.cbDocType.TabIndex = 4;
            // 
            // inputDocNum
            // 
            this.inputDocNum.Location = new System.Drawing.Point(154, 80);
            this.inputDocNum.Name = "inputDocNum";
            this.inputDocNum.Size = new System.Drawing.Size(261, 25);
            this.inputDocNum.TabIndex = 5;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(259, 111);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 148);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.inputDocNum);
            this.Controls.Add(this.cbDocType);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblDesc);
            this.Controls.Add(this.lblDocumentNum);
            this.Controls.Add(this.lblDocumentType);
            this.Name = "Form2";
            this.Text = "Modify";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDocumentType;
        private System.Windows.Forms.Label lblDocumentNum;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ComboBox cbDocType;
        private System.Windows.Forms.TextBox inputDocNum;
        private System.Windows.Forms.Button btnCancel;
    }
}