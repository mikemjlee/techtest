﻿namespace TechTest
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.OpenPDF = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.axAcroPDF1 = new AxAcroPDFLib.AxAcroPDF();
            this.bevel_up = new System.Windows.Forms.Label();
            this.outputFilePath = new System.Windows.Forms.Label();
            this.SavePath = new System.Windows.Forms.Button();
            this.outputTargetPath = new System.Windows.Forms.Label();
            this.checkBoxErrorLog = new System.Windows.Forms.CheckBox();
            this.checkBoxAppLog = new System.Windows.Forms.CheckBox();
            this.listBoxErrorFile = new System.Windows.Forms.ListBox();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF1)).BeginInit();
            this.SuspendLayout();
            // 
            // OpenPDF
            // 
            this.OpenPDF.Location = new System.Drawing.Point(12, 12);
            this.OpenPDF.Name = "OpenPDF";
            this.OpenPDF.Size = new System.Drawing.Size(75, 23);
            this.OpenPDF.TabIndex = 0;
            this.OpenPDF.Text = "OpenPDF";
            this.OpenPDF.UseVisualStyleBackColor = true;
            this.OpenPDF.Click += new System.EventHandler(this.OpenPDF_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Enabled = false;
            this.btnProcess.Location = new System.Drawing.Point(783, 9);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // axAcroPDF1
            // 
            this.axAcroPDF1.Enabled = true;
            this.axAcroPDF1.Location = new System.Drawing.Point(289, 97);
            this.axAcroPDF1.Name = "axAcroPDF1";
            this.axAcroPDF1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAcroPDF1.OcxState")));
            this.axAcroPDF1.Size = new System.Drawing.Size(705, 379);
            this.axAcroPDF1.TabIndex = 2;
            // 
            // bevel_up
            // 
            this.bevel_up.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.bevel_up.Location = new System.Drawing.Point(-2, 81);
            this.bevel_up.Name = "bevel_up";
            this.bevel_up.Size = new System.Drawing.Size(1025, 2);
            this.bevel_up.TabIndex = 5;
            // 
            // outputFilePath
            // 
            this.outputFilePath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outputFilePath.Location = new System.Drawing.Point(93, 12);
            this.outputFilePath.Name = "outputFilePath";
            this.outputFilePath.Size = new System.Drawing.Size(662, 23);
            this.outputFilePath.TabIndex = 6;
            this.outputFilePath.Tag = "outputFilePath";
            // 
            // SavePath
            // 
            this.SavePath.Location = new System.Drawing.Point(12, 41);
            this.SavePath.Name = "SavePath";
            this.SavePath.Size = new System.Drawing.Size(75, 23);
            this.SavePath.TabIndex = 7;
            this.SavePath.Text = "SavePath";
            this.SavePath.UseVisualStyleBackColor = true;
            this.SavePath.Click += new System.EventHandler(this.SavePath_Click);
            // 
            // outputTargetPath
            // 
            this.outputTargetPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outputTargetPath.Location = new System.Drawing.Point(93, 44);
            this.outputTargetPath.Name = "outputTargetPath";
            this.outputTargetPath.Size = new System.Drawing.Size(662, 23);
            this.outputTargetPath.TabIndex = 8;
            this.outputTargetPath.Tag = "outputTargetPath";
            // 
            // checkBoxErrorLog
            // 
            this.checkBoxErrorLog.AutoSize = true;
            this.checkBoxErrorLog.Checked = true;
            this.checkBoxErrorLog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxErrorLog.Location = new System.Drawing.Point(783, 48);
            this.checkBoxErrorLog.Name = "checkBoxErrorLog";
            this.checkBoxErrorLog.Size = new System.Drawing.Size(87, 19);
            this.checkBoxErrorLog.TabIndex = 9;
            this.checkBoxErrorLog.Text = "Error Log";
            this.checkBoxErrorLog.UseVisualStyleBackColor = true;
            this.checkBoxErrorLog.CheckedChanged += new System.EventHandler(this.checkBoxErrorLog_CheckedChanged);
            // 
            // checkBoxAppLog
            // 
            this.checkBoxAppLog.AutoSize = true;
            this.checkBoxAppLog.Checked = true;
            this.checkBoxAppLog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAppLog.Location = new System.Drawing.Point(880, 47);
            this.checkBoxAppLog.Name = "checkBoxAppLog";
            this.checkBoxAppLog.Size = new System.Drawing.Size(80, 19);
            this.checkBoxAppLog.TabIndex = 10;
            this.checkBoxAppLog.Text = "App Log";
            this.checkBoxAppLog.UseVisualStyleBackColor = true;
            this.checkBoxAppLog.CheckedChanged += new System.EventHandler(this.checkBoxAppLog_CheckedChanged);
            // 
            // listBoxErrorFile
            // 
            this.listBoxErrorFile.FormattingEnabled = true;
            this.listBoxErrorFile.ItemHeight = 15;
            this.listBoxErrorFile.Location = new System.Drawing.Point(12, 97);
            this.listBoxErrorFile.Name = "listBoxErrorFile";
            this.listBoxErrorFile.Size = new System.Drawing.Size(254, 409);
            this.listBoxErrorFile.TabIndex = 11;
            this.listBoxErrorFile.SelectedIndexChanged += new System.EventHandler(this.listBoxErrorFile_SelectedIndexChanged);
            // 
            // btnModify
            // 
            this.btnModify.Enabled = false;
            this.btnModify.Location = new System.Drawing.Point(919, 487);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 23);
            this.btnModify.TabIndex = 12;
            this.btnModify.Text = "Modify";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(880, 9);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 13;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 522);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.listBoxErrorFile);
            this.Controls.Add(this.checkBoxAppLog);
            this.Controls.Add(this.checkBoxErrorLog);
            this.Controls.Add(this.outputTargetPath);
            this.Controls.Add(this.SavePath);
            this.Controls.Add(this.outputFilePath);
            this.Controls.Add(this.bevel_up);
            this.Controls.Add(this.axAcroPDF1);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.OpenPDF);
            this.Name = "Form1";
            this.Text = "TechTest";
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OpenPDF;
        private System.Windows.Forms.Button btnProcess;
        private AxAcroPDFLib.AxAcroPDF axAcroPDF1;
        private System.Windows.Forms.Label bevel_up;
        private System.Windows.Forms.Label outputFilePath;
        private System.Windows.Forms.Button SavePath;
        private System.Windows.Forms.Label outputTargetPath;
        private System.Windows.Forms.CheckBox checkBoxErrorLog;
        private System.Windows.Forms.CheckBox checkBoxAppLog;
        private System.Windows.Forms.ListBox listBoxErrorFile;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnReset;
    }
}

