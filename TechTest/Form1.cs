﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TechTest
{
    public partial class Form1 : Form
    {

        private OpenFileDialog m_pOFD = new OpenFileDialog();
        private FolderBrowserDialog m_pFBD = new FolderBrowserDialog();
        private string m_strFileName = @"";
        private string m_strSavePath = @"";
        private DateTime m_pCreatedTimestamp;

        public Form1()
        {
            InitializeComponent();
        }

        private void OpenPDF_Click(object sender, EventArgs e)
        {
            m_pOFD.Filter = "PDF|*.pdf";
            if (m_pOFD.ShowDialog() == DialogResult.OK)
            {
                m_strFileName = m_pOFD.FileName;
                outputFilePath.Text = m_strFileName;

                if (m_strSavePath != "") {
                    btnProcess.Enabled = true;
                }
            }
        }

        private void SavePath_Click(object sender, EventArgs e)
        {
            if (m_pFBD.ShowDialog() == DialogResult.OK)
            {
                m_strSavePath = m_pFBD.SelectedPath + System.IO.Path.DirectorySeparatorChar.ToString();
                outputTargetPath.Text = m_strSavePath;

                if (m_strFileName != "")
                {
                    btnProcess.Enabled = true;
                }
            }

        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (m_strFileName != @"" && m_strSavePath != @"")
            {
                TechTestParser pParser = new TechTestParser(m_strFileName);
                List<string> aryErrorFileList = pParser.ProcessSplitAndSave(m_strSavePath);
                m_pCreatedTimestamp = pParser.FileCreateTime;
                foreach (var strFileName in aryErrorFileList)
                {
                    listBoxErrorFile.Items.Add(strFileName);
                }
            } else {

            }
        }

        private void listBoxErrorFile_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (listBoxErrorFile.SelectedItem != null)
            {
                axAcroPDF1.src = listBoxErrorFile.SelectedItem.ToString();
                btnModify.Enabled = true;
            }
            else {
                axAcroPDF1.LoadFile("none");
            }
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            using (Form2 pModifiedForm = new Form2())
            {
                if (pModifiedForm.ShowDialog() == DialogResult.OK)
                {
                    string strDocumentType = pModifiedForm.DocumentType;
                    string strDocumentNumber = pModifiedForm.DocumentNumber;

                    string strFileName = TechTestParser.GetFileName(strDocumentType, strDocumentNumber, m_pCreatedTimestamp);
                    string strDestFolderPath = TechTestParser.GetFolderPath(m_strSavePath, strDocumentType, strDocumentNumber, m_pCreatedTimestamp);
                    string strXMLPath = Path.GetDirectoryName(listBoxErrorFile.SelectedItem.ToString()) + System.IO.Path.DirectorySeparatorChar.ToString() + Path.GetFileNameWithoutExtension(listBoxErrorFile.SelectedItem.ToString()) + ".xml";

                    try
                    {
                        TechTestLog.Instance.Log(TechTestLog.LOGLV, "Move PDF file from " + listBoxErrorFile.SelectedItem.ToString() + " to " + strDestFolderPath + strFileName + ".pdf");

                        string strPDFFilePath = strDestFolderPath + strFileName + ".pdf";
                        if (File.Exists(strPDFFilePath))
                        {
                            File.Delete(strPDFFilePath);
                        }
                        File.Move(listBoxErrorFile.SelectedItem.ToString(), strPDFFilePath);
                    }
                    catch (IOException ex)
                    {
                        TechTestLog.Instance.Log(TechTestLog.ERRLV, "Move PDF file failed ({0})", ex);
                    }
                    try {

                        string strXMLFilePath = strDestFolderPath + strFileName + ".xml";
                        if (File.Exists(strXMLFilePath))
                        {
                            File.Delete(strXMLFilePath);
                        }
                        TechTestLog.Instance.Log(TechTestLog.LOGLV, "Move XML file from " + strXMLPath + " to " + strXMLFilePath);
                        File.Move(strXMLPath, strXMLFilePath);
                    }
                    catch (IOException ex)
                    {
                        TechTestLog.Instance.Log(TechTestLog.ERRLV, "Move XML file failed ({0})", ex);
                    }
                    btnModify.Enabled = false;
                    listBoxErrorFile.Items.Remove(listBoxErrorFile.SelectedItem);
                }
            }
        }

        private void checkBoxErrorLog_CheckedChanged(object sender, EventArgs e)
        {
            int nLogLevel = ((checkBoxAppLog.Checked) ? 1 : 0) << TechTestLog.LOGLV | ((checkBoxErrorLog.Checked) ? 1 : 0) << TechTestLog.ERRLV;
            TechTestLog.Instance.SetLogLevel(nLogLevel);
        }

        private void checkBoxAppLog_CheckedChanged(object sender, EventArgs e)
        {
            int nLogLevel = ((checkBoxAppLog.Checked) ? 1 : 0) << TechTestLog.LOGLV | ((checkBoxErrorLog.Checked) ? 1 : 0) << TechTestLog.ERRLV;
            TechTestLog.Instance.SetLogLevel(nLogLevel);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnModify.Enabled = false;
            checkBoxAppLog.Checked = true;
            checkBoxErrorLog.Checked = true;
            m_strFileName = @"";
            m_strSavePath = @"";
            listBoxErrorFile.Items.Clear();
            axAcroPDF1.LoadFile("none");
            outputFilePath.Text = m_strFileName;
            outputTargetPath.Text = m_strSavePath;
            btnProcess.Enabled = false;
        }
    }
}
